#!/usr/bin/env zsh
# vim:ft=zsh

zplug_init() {
    export ZPLUG_HOME=${HOME}/.zplug
    if [[ $OSTYPE == *darwin* ]]; then
        export ZPLUG_CACHE_DIR=${HOME}/Library/Caches/zplug
        export ZPLUG_REPOS="${HOME}/Library/Application Support/zplug"
    else
        export ZPLUG_CACHE_DIR=${HOME}/.cache/zplug
        export ZPLUG_REPOS="${HOME}/.local/zplug"
    fi

    if [[ ! -d ${ZPLUG_HOME} ]]; then
        rm -rf ${ZPLUG_HOME}
        if (( $+commands[git] )); then
            git clone --single-branch https://github.com/eirnym/zplug.git $ZPLUG_HOME
        else
            return 1
        fi
    fi

    export ZPLUG_LOG_LOAD_SUCCESS=false
    export ZPLUG_LOG_LOAD_FAILURE=false
    export ZPLUG_LOG_TRACE=false

    source ~/.zplug/init.zsh
}

zplug_plugins() {

    [[ -f $ZDOTDIR/.zplug.plugins ]] && . $ZDOTDIR/.zplug.plugins
    [[ -f $ZDOTDIR/.zplug.plugins.local ]] && . $ZDOTDIR/.zplug.plugins.local
}

zplug_apply() {
    if [[ -o login ]]; then
        zplug install 2>/dev/null
    fi
    zplug load
}

if zplug_init; then
    zplug_plugins
    zplug_apply
fi
