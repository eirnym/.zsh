if (( $+commands[terminfo] )); then
    if ! {terminfo >/dev/null 2>&1 }; then
        export TERM=xterm-256color
    fi
fi

export BLOCKSIZE=K;
export EDITOR=vi;
export PAGER=less;

# set ENV to a file invoked each time sh is started for interactive use.
export ZDOTDIR="$HOME/.zsh";
export ENV=$ZDOTDIR/.zshrc;

load_home_paths() {
    zmodload zsh/mapfile || return 0
    setopt LOCAL_OPTIONS RC_EXPAND_PARAM
    local filename=${ZSH_LOAD_PATHS:-${ZDOTDIR}/.paths}
    if [[ ! -e $filename ]]; then
        _home_paths=($_home_paths $HOME/bin $HOME/.local/bin )
        return
    fi
    local home_paths=(${(f@)mapfile[$filename]})   # read lines from file
    home_paths=(${home_paths%%[#]*})               # remove comments
    home_paths=(${home_paths%%[[:space:]]*})       # remove empty lines and extra spaces
    local abs_paths=(${(M)home_paths:#/*})         # select absolute addresses
    local rel_paths=(${HOME}/${home_paths:#/*})    # add $HOME to relative
    _home_paths=( $_home_paths $abs_paths $rel_paths ) # export
    return 0
}

load_exec_paths() {
    zmodload zsh/mapfile || return 0
    setopt LOCAL_OPTIONS RC_EXPAND_PARAM
    local filename=${ZSH_LOAD_PATHS:-${ZDOTDIR}/.paths.exec}
    if [[ ! -e $filename ]]; then
        return 0
    fi
    local line
    local home_paths=(${(f@)mapfile[$filename]})   # read lines from file
    home_paths=(${home_paths%%[#]*})               # remove comments
    home_paths=(${home_paths##[[:space:]]*})       # remove empty lines and extra spaces
    for item in "${(@)home_paths}"; do
        value=$( eval $item || true)
        _home_paths=( $_home_paths $value  )
    done
    return 0

}
_home_paths=()
load_home_paths
load_exec_paths

export PATH=${(j,:,)_home_paths}:$PATH
export ZPROFILE_DONE=true
