if [[ $TERM == 'linux' || $TERM == 'screen' ]]; then
    export TERM=xterm-256color
fi

export LESS="-igsj2 -z -2 -P=%pB\% of ?f%f:<stdin>.(file %i of %m)\: byte %bb/%B; line %lb/%L\. [Next %x]\$PM?f%f:<stdin>."

if [[ -z $EDITOR ]]; then
    export EDITOR=vim
fi
if [[ -z $TERMINAL_EDITOR ]]; then
    export TERMINAL_EDITOR=vim
fi

export LS_COLORS="di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32";
export LSCOLORS="ExGxFxDxCxDxDxhbhdacEc";
export LISTMAX=200
export LESSLINES=300
export HISTSIZE=10000000
export SAVEHIST=10000000
export HISTFILE=${ZDOTDIR}/.zsh_history
export PAGER=less

if [[ -d ~/.terminfo ]]; then
    export TERMINFO=~/.terminfo
fi

export DEV_SDK_FOLDER="${HOME}/Developer/sdk"

export BUN_INSTALL="${DEV_SDK_FOLDER}/bun"
export NVM_DIR="${DEV_SDK_FOLDER}/nodejs"
export RBENV_ROOT="${DEV_SDK_FOLDER}/rbenv"
export PYTHON_SDK_PREFIX="${DEV_SDK_FOLDER}/python"
export ANDROID_HOME="${DEV_SDK_FOLDER}/android"

export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_NO_AUTO_UPDATE=1
export HOMEBREW_AUTO_UPDATE_CHECKED=1

export PUPPETEER_PRODUCT=firefox
export PUPPETEER_SKIP_DOWNLOAD=true

export SVN_EDITOR=${TERMINAL_EDITOR}
export HGEDITOR=${TERMINAL_EDITOR}
export GIT_EDITOR=${TERMINAL_EDITOR}
export PGEDITOR=${TERMINAL_EDITOR}
export GIT_PAGER='LESS="-S -R $LESS" less'

export PGSSLMODE=prefer
export PIP_REQUIRE_VIRTUALENV=true

if [[ -z $LANG ]]; then
    export LANG='pl_PL.UTF-8'
fi

if (( $+commands[ilsore-format] )); then
    export VIRTUAL_ENV_DISABLE_PROMPT=1
    export PS1='$(ilsore-format --last-exit-status=$?)'
fi
