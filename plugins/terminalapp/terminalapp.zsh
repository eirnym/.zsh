# Set Apple Terminal.app resume directory
# based on this answer: http://superuser.com/a/315029
# 2012-10-26: (javageek) Changed code using the updated answer

# Tell the terminal about the working directory whenever it changes.
if [[ "$TERM_PROGRAM" == "Apple_Terminal" ]] && [[ -z "$INSIDE_EMACS" ]]; then
  update_terminal_cwd() {
        # Identify the directory using a "file:" scheme URL, including
        # the host name to disambiguate local vs. remote paths.

        # Use LANG=C to process text byte-by-byte.
        local LANG=C
        local URL_PATH=$(echo "${PWD}"|perl -ple 's/([^\/._~A-Za-z0-9-])/sprintf "%%%02x", ord($1) /ge')
        local PWD_URL="file://$HOST$URL_PATH"
        # echo "$PWD_URL"        # testing
        printf '\e]7;%s\a' "$PWD_URL"
    }

    # Register the function so it is called whenever the working
    # directory changes.
    autoload add-zsh-hook
    add-zsh-hook chpwd update_terminal_cwd

    # Tell the terminal about the initial directory.
    update_terminal_cwd
fi
