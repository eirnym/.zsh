#!/usr/bin/env zsh

# ilsore.zsh-theme
#
# Author: Eir Nym
# URL: http://eroese.org/
# Repo:
# Direct link:
# Create:
# Modified:

if ! (( ${+colors} )); then
    autoload -Uz colors
    colors
fi

___RESET_COLOR="%{$reset_color%}"
export VIRTUAL_ENV_DISABLE_PROMPT=1

function virtualenv_prompt_info() {
    if [[ -n $VIRTUAL_ENV ]]; then
        printf "[%s%s%s]" "%{${fg_bold[yellow]}%}" ${${VIRTUAL_ENV}:h:t}/${${VIRTUAL_ENV}:t} "%{$reset_color%}"
    fi
}

PRMT_TIME='[%{%F{165}%D %F{226}%}%*%{$reset_color%}]'
PRMT_USER='%{%F{214}%}%U%n%u%{$reset_color%}@%{%F{46}%}%M%{$reset_color%}'
PRMT_ERR='%(?,,%{${fg_bold[white]}%}[%?]%{$reset_color%})'
PRMT_PATH='%{%F{87}%}%~%{$reset_color%}'
PROMPT="${PRMT_TIME}${PRMT_USER}${PRMT_ERR}"'$(virtualenv_prompt_info)$(vcs_prompt_ilsore)
'"${PRMT_PATH}>"
unset PRMT_USER
unset PRMT_TIME
unset PRMT_ERR
unset PRMT_PATH
