#
#  vcs options:
#   * branch
#   * index_status
#   * untracked
#   * ahead
#
#

ZSH_THEME_VCS_PROMPT_PREFIX="(%{$fg[magenta]%}"
ZSH_THEME_VCS_PROMPT_BRANCH="%{${fg_bold[yellow]}%}"
ZSH_THEME_VCS_PROMPT_AHEAD="%{$fg_bold[magenta]%}↑%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_BACKWARDS="%{$fg_bold[green]%}↓%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_STAGED="%{$fg_bold[green]%}●%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_UNSTAGED="%{$fg_bold[red]%}●%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_UNTRACKED="%{$fg_bold[red]%}?%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_UNMERGED="%{$fg_bold[red]%}✘%{$reset_color%}"
ZSH_THEME_VCS_PROMPT_SUFFIX=")"

function vcs_prompt_ilsore {
    local output=$(vcs_prompt_processing)

    if [[ -z $output ]]; then
        return
    fi
    local opts
    typeset -A opts
    echo "$output" | while read line; do
        a=("${(@s/=/)line}")
        name=$a[1]
        value=$a[2]
        opts[$name]="$value"
    done
    local space=true
    # prefix
    echo -n "$ZSH_THEME_VCS_PROMPT_PREFIX${opts[vcs_type]}: "
    # branch or revision
    echo -n "${ZSH_THEME_VCS_PROMPT_BRANCH}${opts[branch]}${opts[revision]}%{$reset_color%}"
    # ahead/backwards
    if [[ -n ${opts[up]} ]]; then
        if $space; then echo -n ' '; space=false fi
        echo -n "$ZSH_THEME_VCS_PROMPT_AHEAD"
    fi
    if [[ -n ${opts[down]} ]]; then
        if $space; then echo -n ' '; fi
        echo -n "$ZSH_THEME_VCS_PROMPT_BACKWARDS"
    fi
    space=true
    if [[ -n ${opts[staged]} ]]; then
        if $space; then echo -n ' '; space=false fi
        echo -n "$ZSH_THEME_VCS_PROMPT_STAGED"
    fi
    if [[ -n ${opts[unstaged]} ]]; then
        if $space; then echo -n ' '; space=false fi
        echo -n "$ZSH_THEME_VCS_PROMPT_UNSTAGED"
    fi
    if [[ -n ${opts[unmerged]} ]]; then
        if $space; then echo -n ' '; space=false fi
        echo -n "$ZSH_THEME_VCS_PROMPT_UNMERGED"
    fi
    if [[ -n ${opts[untracked]} ]]; then
        if $space; then echo -n ' '; space=false fi
        echo -n "$ZSH_THEME_VCS_PROMPT_UNTRACKED"
    fi
    echo -n "$ZSH_THEME_VCS_PROMPT_SUFFIX"
}

function git_vcs_prompt_ilsore {
    local options="$@"
    local ref_output
    local git_branch=$(git symbolic-ref HEAD 2>/dev/null | sed -n 's,^refs/heads/,,p')
    if [[ -z $git_branch ]]; then
        ref_output=$(git rev-parse --short HEAD 2>/dev/null) || return
    fi
    local vcs_status
    typeset -A vcs_status

    vcs_status[branch]=$git_branch
    vcs_status[revision]=$ref_output
    vcs_status[staged]=
    vcs_status[unstaged]=
    vcs_status[untracked]=
    vcs_status[unmerged]=

    vcs_status[up]=
    vcs_status[down]=

    if [[ -z $git_branch ]]; then
        echo "revision=$ref_output"
        return
    fi

    local show_index_status=$(( ${options[(i)index_status]} <= ${#options} ))
    if [[ $show_index_status -eq 1 ]]; then
        local show_untracked=$(( ${options[(i)untracked]}    <= ${#options} ))
        if [[ $show_untracked -eq 1 ]]; then
            show_untracked='normal'
        else
            show_untracked='no'
        fi
        local INDEX
        typeset -a INDEX
        INDEX=($( \
            git status --porcelain=v2 --ignored=no --untracked-files=$show_untracked --column \
            | cut -c 1-4| \
            sed -n 's,^[?] ..,n,p;
                    s,^[u] ..,u,p;
                    s,^[12] [A-Z][.]$,s,p;
                    s,^[12] [.][A-Z]$,c,p;
                    s,^[12] [A-Z][A-Z]$,s|c,p;' \
            | tr '|' '\n' ))
        vcs_status[staged]=$((${INDEX[(i)s]} <= $#INDEX ))
        vcs_status[unstaged]=$((${INDEX[(i)c]} <= $#INDEX ))
        vcs_status[untracked]=$((${INDEX[(i)n]} <= $#INDEX ))
        vcs_status[unmerged]=$((${INDEX[(i)u]} <= $#INDEX ))
    fi


    if [[ $(( ${options[(i)ahead]} <= ${#options} )) -eq 1 ]]; then
        local tracking_branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || true)
        if [[ -n $tracking_branch ]]; then
            git rev-list --left-right --count $tracking_branch...HEAD | \
                    while read down up; do
                        vcs_status[down]=$(( $down > 0))
                        vcs_status[up]=$(( $up > 0 ))
                    done
        fi
    fi

    for k in "${(@k)vcs_status}"; do
        if [[ -n $vcs_status[$k] ]]; then
            if [[ $vcs_status[$k] != 0 ]]; then
              echo "$k=$vcs_status[$k]"
            fi
        fi
    done
}

function hg_prompt_info3 {
    if ! (( $+commands[fast-hg-bookmark] )); then
        return;
    fi

    local _PRMT_BRANCH
    _PRMT_BRANCH=$(fast-hg-bookmark)

    if [[ -z  "$_PRMT_BRANCH" ]]; then
        return
    fi
    echo "branch=${_PRMT_BRANCH}"
}

function local_upfind {
    while [[ $PWD != / ]] ; do
        result=$(find "$PWD" -depth 1 "$@" 2>/dev/null | head -n 1 )
        if [[ $? != 0 ]]; then
            return $?
        fi
        if [ -n "$result" ]; then
            echo $result
            return 0
        fi
        cd ..
    done

    return 1
}

function vcs_prompt_processing {
    local vcs_folder=''
    local vcs_prompt_settings
    typeset -a vcs_prompt_settings

    [[ -n "$NO_VCS_PROMPT" ]] && return

    if (( $+commands[upfind] )); then
        upfind=local_upfind
    fi

    vcs_folder=$(upfind .git)

    if [[ $? != 0 ]]; then
        return;
    fi

    if [[ -f "$vcs_folder/.no_vcs_prompt" ]]; then
        return;
    fi

    if [[ -f "$vcs_folder/.vcs_prompt_opts" ]]; then
        cat "$vcs_folder/.vcs_prompt_opts" | while read option; do
            if [[ -z $option ]]; then
                continue
            fi
            case $option in
                skip) return ;; # no vcs prompt

                branch)    vcs_prompt_settings=($vcs_prompt_settings branch) ;;
                ahead)    vcs_prompt_settings=($vcs_prompt_settings ahead) ;;
                index_status)  vcs_prompt_settings=($vcs_prompt_settings index_status) ;;
                untracked) vcs_prompt_settings=($vcs_prompt_settings untracked) ;;

                *) if [[ -n $VCS_PROMPT_VERBOSE ]]; then
                    echo "vcs prompt option '$option' is not supported" >&2
                    return 1
                fi ;;
            esac
        done
    else
        vcs_prompt_settings=(branch index_status untracked ahead)
    fi

    case $vcs_folder in
        *.git*) echo 'vcs_type=Git'; git_vcs_prompt_ilsore $vcs_prompt_settings  ;;
        *.hg*) echo 'vcs_type=Hg'; hg_prompt_info3 $vcs_prompt_settings ;;
    esac
}
