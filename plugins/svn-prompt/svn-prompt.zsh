# VCS type
# VCS remotes
# Branch
# Revision
# root path
# Modification statuses [addition] [deletion] [modifications] [replacements] [untracked] [dirty]
# project from remotes:
#   * bitbucket
#   * github
#   * FreeBSD
#   * SourceForge
#   * Google Code
#

typeset -A _svn_statuses

_svn_statuses[addition]='^\s*A'
_svn_statuses[deletion]='^\s*D'
_svn_statuses[modifications]='^\s*M'
_svn_statuses[replacements]='^\s*[R~]'
_svn_statuses[untracked]='^\s*\?'
_svn_statuses[dirty]='^\s*[CI!L]'

function new_svn_prompt_info() {
    local vcs_info
    local vcs_status
    
    local repo_url
    local repo_root
    local branch
    local revision

    local st

    vcs_info=$(svn info 2>/dev/null) || return 1
    vcs_status=$(svn status 2>/dev/null)

    repo_root=$(sed -n 's/^Repository\ Root: \(.*\)$/\1/p' <<< ${vcs_info} )
    branch=$(sed -En '/URL:/s,^.*((tags|branches)/[^/]+|trunk).*,\1,p' <<< "${vcs_info}")
    revision=$(sed -n 's/^Revision: \(.*\)$/\1/p' <<< "${vcs_info}" )
    repo_url=$(sed -n 's/^URL: \(.*\)$/\1/p' <<< "${vcs_info}" )
    root_path=$(sed -n 's/^Working Copy Root Path: \(.*\)$/\1/p' <<< "${vcs_info}" )
    
#    echo XXX $repo_root $repo_url $branch $revision
#    typeset -A VCS_PROMPT_INFO

    VCS_PROMPT_INFO[vcs]="svn"
    VCS_PROMPT_INFO[branch]=$branch
    VCS_PROMPT_INFO[root_path]=$root_path
    VCS_PROMPT_INFO[revision_number]="$revision"
    VCS_PROMPT_INFO[revision_short]=
    VCS_PROMPT_INFO[revision_long]=
#    typeset -A VCS_REMOTES
    VCS_REMOTES[url]="$repo_url"
    VCS_REMOTES[root]="$repo_root"

    for st in ${(@k)_svn_statuses}; do
        if grep -q -m 1 "${_svn_statuses[$st]}" <<< $vcs_status; then
            VCS_STATUSES[$st]=$result
        fi
    done
}


function dump_svn_prompt_info() {
    local VCS_PROMPT_INFO
    local VCS_STATUSES
    local VCS_REMOTES

    typeset -A VCS_PROMPT_INFO
    typeset -A VCS_STATUSES
    typeset -A VCS_REMOTES
    new_svn_prompt_info || return 1

    echo
    echo info
    for k in "${(@k)VCS_PROMPT_INFO}"; do
        echo "$k -> $VCS_PROMPT_INFO[$k]"
    done
    echo
    echo statuses
    for k in "${(@k)VCS_STATUSES}"; do
        echo "$k -> $VCS_STATUSES[$k]"
    done
    echo
    echo remotes
    for k in "${(@k)VCS_REMOTES}"; do
        echo "$k -> $VCS_REMOTES[$k]"
    done
}
