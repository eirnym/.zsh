if [[ -n $ZSH_LOAD_PROFILE ]]; then
    zmodload zsh/datetime
    setopt PROMPT_SUBST
    ORIG_PS4=$PS4
    PS4='+$EPOCHREALTIME %N:%i> '

    logfile=$(mktemp $ZDOTDIR/zsh_profile.XXXXXXXX)
    echo "Logging to $logfile"
    exec 3>&2 2>$logfile
    setopt XTRACE
fi

if (( $+commands[terminfo] )); then
    if ! {terminfo >/dev/null 2>&1 }; then
        export TERM=xterm-256color
    fi
fi

# bracket all urls
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

ZSH_PLUGIN_MANAGER=zplug

_scripts=(.zshenv .zsh_fun .${ZSH_PLUGIN_MANAGER}.rc .zsh_options )
_scripts=($_scripts .zsh_bind2 .zsh_dir_aliases .zsh_aliases .zsh_compinit)

for script in "${(@)_scripts}"; do
    [[ -f $ZDOTDIR/${script} ]] && . $ZDOTDIR/${script}
    [[ -f $ZDOTDIR/${script}.local ]] && . $ZDOTDIR/${script}.local
done

unset ZSH_PLUGIN_MANAGER_NEW
unset ZSH_PLUGIN_MANAGER
unset _scripts
unset script

if [[ -n $ZSH_LOAD_PROFILE ]]; then
    unsetopt XTRACE
    exec 2>&3 3>&-
    PS4=$ORIG_PS4
fi
